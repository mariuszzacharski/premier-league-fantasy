from urllib.request import Request, urlopen
import json

# "https://fantasy.premierleague.com/api/element-summary/[player_id]/"
# "https://fantasy.premierleague.com/api/bootstrap-static/"

def json_load_from_url(player_id = None):
    if player_id != None:
        url = "https://fantasy.premierleague.com/api/element-summary/{player_id}/"
        url = url.format(player_id=player_id)
    else:
        url = "https://fantasy.premierleague.com/api/bootstrap-static/"
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urlopen(req).read()
    json_loaded = json.loads(webpage)
    return json_loaded
import base64
import io
from PIL import Image, ImageDraw, ImageFont
import csv
import requests

def _download_image_from_pl(code):
    url = "https://resources.premierleague.com/premierleague/photos/players/110x140/p" + str(code) + ".png"
    img_data = requests.get(url).content
    with open(str(code) + ".png", 'wb') as handler:
        handler.write(img_data)

    return str(code) + ".png"
def return_img_bytes():
    with open("EmptyPitch.jpg", "rb") as image:
        img = base64.b64encode(image.read())
    return img

image = Image.open(io.BytesIO(base64.b64decode(return_img_bytes())), 'r')
draw = ImageDraw.Draw(image)
color = "rgb(255,255,255)"

proporc = 220 / 280
new_height = 100
new_value = int(new_height / proporc)
box = Image.open("box.png")

images_dict = {

    "goalKeepers_1": {"photo_loc": (432, 5),
                      "box_loc": (416, 125),
                      "surname": (416, 125),
                      "stats": (422, 151)},

    "defenders_1": {"photo_loc": (150, 170),
                    "box_loc": (134, 290),
                    "surname": (134, 290),
                    "stats": (140, 316)},

    "defenders_2": {"photo_loc": (432, 170),
                    "box_loc": (416, 290),
                    "surname": (416, 290),
                    "stats": (422, 316)},

    "defenders_3": {"photo_loc": (714, 170),
                    "box_loc": (698, 290),
                    "surname": (698, 290),
                    "stats": (704, 316)},

    "midfielders_1": {"photo_loc": (70, 350),
                      "box_loc": (54, 470),
                      "surname": (54, 470),
                      "stats": (60, 496)},

    "midfielders_2": {"photo_loc": (300, 350),
                      "box_loc": (284, 470),
                      "surname": (284, 470),
                      "stats": (290, 496)},

    "midfielders_3": {"photo_loc": (580, 350),
                      "box_loc": (564, 470),
                      "surname": (564, 470),
                      "stats": (572, 496)},


    "midfielders_4": {"photo_loc": (810, 350),
                      "box_loc": (794, 470),
                      "surname": (794, 470),
                      "stats": (800, 496)},

    "forwards_1": {"photo_loc": (150, 515),
                   "box_loc": (134, 635),
                   "surname": (134, 635),
                   "stats": (140, 661)},

    "forwards_2": {"photo_loc": (432, 515),
                   "box_loc": (416, 635),
                   "surname": (416, 635),
                   "stats": (422, 661)},

    "forwards_3": {"photo_loc": (714, 515),
                   "box_loc": (698, 635),
                   "surname": (698, 635),
                   "stats": (704, 661)}
}

cnt = 0
font_name = ImageFont.truetype("Calibri Bold.ttf", size=20)
font_stats = ImageFont.truetype("Calibri Bold.ttf", size=10)

total_points = 0
team_cost = 0

with open("PredFrame.csv", "r") as file:
    csv_file = csv.reader(file)
    if cnt == 0:
        for row in csv_file:
            print(row)
            if (row[0] != "second_first_name") \
                    & (((row[3] + "_" + row[7]) == "goalKeepers_1")
                      | ((row[3] + "_" + row[7]) == "defenders_1") | ((row[3] + "_" + row[7]) == "defenders_2") | ((row[3] + "_" + row[7]) == "defenders_3")
                      | ((row[3] + "_" + row[7]) == "midfielders_1") | ((row[3] + "_" + row[7]) == "midfielders_2") | ((row[3] + "_" + row[7]) == "midfielders_3")
                      | ((row[3] + "_" + row[7]) == "midfielders_4")
                      | ((row[3] + "_" + row[7]) == "forwards_1") | ((row[3] + "_" + row[7]) == "forwards_2") | ((row[3] + "_" + row[7]) == "forwards_3")


            ):
                dict_key = row[3] + "_" + row[7]
                dict_value = images_dict.get(dict_key)
                print(row, dict_value.get("surname"))
                cnt += 1

                dict_value.get("surname")
                total_points += int(row[5])
                team_cost += int(row[6])
                player_photo = Image.open(_download_image_from_pl(int(row[2])))
                player_photo = player_photo.resize((new_height, new_value))
                image.paste(player_photo, (dict_value.get("photo_loc")), player_photo)
                image.paste(box, (dict_value.get("box_loc")), box)
                draw = ImageDraw.Draw(image)

                surname = row[1]

                if len(surname) > 13:
                    surname = surname[: 11] + " (...)"

                draw.text(dict_value.get("surname"), surname, fill=color, align="center", font=font_name)
                draw.text(dict_value.get("stats"), "Total earned points: " + row[5], fill=(0, 0, 0), align="center", font=font_stats)




draw = ImageDraw.Draw(image)
draw.text((155, 65), "Points " + str(total_points), fill=(255, 255, 255), align="left", font=font_name)
draw.text((670, 65), "Value " + str(team_cost / 10) + " £", fill=(255, 255, 255), align="left", font=font_name)

image.save("Squad.jpg")
from DownloadDataFromAPI import json_load_from_url
import pandas
from statsmodels.tsa.statespace.sarimax import SARIMAX
from sklearn.model_selection import train_test_split
from config_folder.UploadDataToGSheets import upload_data_g_sheets
# from pyramid.arima import auto_arima

def _generate_next_gameweeks_(gameWeekFrame):
    len_dataframe = len(gameWeekFrame)

    next_frames = []

    for gameweek in range(len_dataframe + 1, 39):
        frame = pandas.DataFrame(["Gameweek " + str(gameweek)], columns=["GameweekName"])
        next_frames.append(frame)

    return pandas.concat(next_frames, ignore_index=True)


OverallJSON = json_load_from_url()
GameweekList = [stat for stat in OverallJSON.get("events")]

GameweekLists = []

for GameWeek in GameweekList:
    try:
        if (GameWeek.get("finished") == True) & (GameWeek.get("highest_score") > 0):
            GameweekList = pandas.DataFrame([[GameWeek.get("highest_score"), GameWeek.get("name")]], columns=["highestScore", "GameweekName"])
            GameweekLists.append(GameweekList)
    except:
        pass

GameweekFrames = pandas.concat(GameweekLists, ignore_index=True)
highest_score = GameweekFrames.highestScore.values
y_train, y_test = train_test_split(highest_score, test_size=0.33)

PredList = []

model = auto_arima(highest_score, trace=True, error_action="ignore", suppress_warnings=True, m=12)
model_SARIMA = SARIMAX(highest_score, order=(3, 2, [1, 2]))

results = model_SARIMA.fit()
predictions = results.predict(start=1, end=38)

GameweekFinal = GameweekFrames.append(_generate_next_gameweeks_(GameweekFrames)).reset_index().drop(columns=["index"])
GameweekFinal["Mean"] = int(GameweekFinal["highestScore"].mean())
GameweekFinal["highestScore"] = GameweekFinal["highestScore"].astype(str).str.split(".").str[0]
GameweekFinal["highScorePredicted"] = predictions
GameweekFinal["highScorePredicted"] = GameweekFinal["highScorePredicted"].astype(int)
GameweekFinal["Gameweek Number"] = GameweekFinal.index + 1

GameweekFinal = GameweekFinal[["highestScore", "highScorePredicted", "Mean", "GameweekName", "Gameweek Number"]].fillna("")

upload_data_g_sheets(GameweekFinal, "GameweekFrame")

from DownloadDataFromAPI import json_load_from_url
from sklearn.cluster import KMeans
import pandas

def get_dict_attribute(_list_, attrib_name):

    _list_converted = list(_list_)
    loopedList = _list_converted
    loop_cnt = 1
    attrib_dicts = {}

    while len(loopedList) != 0:

        attrib_dict = {
            list(_list_).index(max(loopedList)): str(loop_cnt) + " najwyższy wynik " + attrib_name}
        attrib_dicts.update(attrib_dict)

        # print(_list_.index(max(loopedList)), str(loop_cnt) + " najwyższy wynik " + attrib_name)
        # print(max(loopedList), list(_list_))
        loopedList.remove(max(loopedList))
        loop_cnt += 1

    return attrib_dicts


def get_stats_from_last_games(player_id,
                              lookBack=10,
                              analisis_type=None
                              ):

    if lookBack != None:
        player_history = pandas.DataFrame.from_records([stat for stat in json_load_from_url(player_id=player_id).get("history")])[-lookBack:]
    else:
        player_history = pandas.DataFrame.from_records(
            [stat for stat in json_load_from_url(player_id=player_id).get("history")])

    if analisis_type == None:
        return player_history
    else:
        if analisis_type == "sum_of_points":
            player_history = player_history.groupby(["element"])["total_points"].sum()
        elif analisis_type == "sum_of_points_home":
            player_history = player_history[player_history["was_home"] == True]
            player_history = player_history.groupby(["element"])["total_points"].sum()

        return player_history






OverallJSON = json_load_from_url()
PlayersFrame = [stat for stat in OverallJSON.get("elements")]

goalKeepers = pandas.DataFrame.from_records(PlayersFrame)
goalKeepers = goalKeepers[(goalKeepers["element_type"] == 1) & (goalKeepers["total_points"] > 0)]

# TopBestGoalKeepers

TopGoalKeepers = goalKeepers\
    .groupby(["second_name", "first_name", "id"])\
    ["total_points", "now_cost"].sum()\
    .reset_index()\
    .sort_values(["total_points"], ascending=False)

TopGoalKeepers["PointsPerValue"] = TopGoalKeepers["total_points"] / TopGoalKeepers["now_cost"]

kmeans = KMeans(n_clusters=5, init='k-means++', max_iter=300, n_init=10, random_state=0)
pred_y = kmeans.fit_predict(TopGoalKeepers[["total_points", "now_cost"]])

TopGoalKeepers["Cluster"] = pred_y
TopGoalKeepers["LastTenGames"] = TopGoalKeepers["id"].apply(lambda x: get_stats_from_last_games(player_id=x, analisis_type="sum_of_points").values[0])


get_stats_from_last_games(player_id=19)
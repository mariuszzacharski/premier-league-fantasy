## Premier League Fantasy

**What is about?**

*Leagues. After entering their squad into the game, managers can join and create 
leagues to compete with friends and others across the world. Private leagues are 
where you compete against your friends. Just create a league and then send out the 
unique code to allow your friends to join.*

## Data

Data used in this project comming from official PremierLeague fantasy API 
static files in JSON format which are based on links below: 

* "https://fantasy.premierleague.com/api/bootstrap-static/"
* "https://fantasy.premierleague.com/api/element-summary/[player_id]/"

---
## Used technology
In this project was used Python with external libraries: 

* pandas
* sklearn
* statsmodels
* pygsheets

Data were store on Google Spreadsheets via Google API v4
Visualized on https://datastudio.google.com/reporting/5c44b27e-7d69-4e51-aace-83c64076c4ae
import itertools
from TopPlayers import PlayersFrame
import datetime
import pandas
from config_folder.UploadDataToGSheets import upload_data_g_sheets

Team = PlayersFrame[["id",
                      "element_type",
                      "total_points",
                     "team", "now_cost", "code"]]

cnt = 0
StartDate = datetime.datetime.today()
list_of_combinations = []

for element in [4, 3, 2, 1]:
    element_type = {1: 1, 2: 3, 3: 4, 4: 3}

    TeamFiltered = Team[Team["element_type"] == element].sort_values(["total_points"], ascending=False)[: 10]
    Teams_Dict = TeamFiltered.to_dict('records')

    for n in itertools.combinations(Teams_Dict, element_type.get(element)):
        sum_of_total_point = 0
        sum_of_now_cost = 0
        list_of_id = []

        team_frame = {}

        for dict in n:
            cnt += 1
            if team_frame.get(dict.get("team")) == None:
                team_frame.update({dict.get("team"): 1})

            elif team_frame.get(dict.get("team")) != None:
                value = team_frame.get(dict.get("team")) + 1
                team_frame.update({dict.get("team"): value})

            if (team_frame.get(dict.get("team")) < 3) | (team_frame.get(dict.get("team")) != None):
                list_of_id.append(dict.get("id"))
                sum_of_total_point += dict.get("total_points")
                sum_of_now_cost += dict.get("now_cost")

        cnt += 1
        print(datetime.datetime.today() - StartDate, cnt)
        # print(list_of_id, sum_of_total_point, sum_of_now_cost, avg_points_values, element)
        avg_points_values = sum_of_total_point / sum_of_now_cost
        frame = [list_of_id, sum_of_total_point, sum_of_now_cost, avg_points_values, element]
        list_of_combinations.append(frame)

cnt = 0
FinalCombinations = []

for combination in itertools.combinations(list_of_combinations, 4):
    ListOfPositions = []
    SumOfValues = 0

    for number in range(0, 4):
        ListOfPositions.append(combination[number][4])
        SumOfValues += combination[number][2]

    ListOfPositions.sort()
    if ([1, 2, 3, 4] == ListOfPositions) & (SumOfValues < 900):
        cnt += 1
        FinalCombinations.append(combination)
        if cnt > 0:
            break


PlayersDict = PlayersFrame.set_index("id").to_dict("index")
PositionDict = {1: "goalKeepers",
                2: "defenders",
                3: "midfielders",
                4: "forwards"}

PredFrame = pandas.DataFrame()

for combination in FinalCombinations:
    for n in range(0, 4):
        for player in range(0, len(combination[n][0])):
            player_dict = PlayersDict.get(combination[n][0][player])
            second_first_name = player_dict.get("first_name") + " " + player_dict.get("second_name")
            second_name = player_dict.get("second_name")
            position = PositionDict.get(combination[n][4])
            total_points = player_dict.get("total_points")
            now_costs = player_dict.get("now_cost")
            code = player_dict.get("code")
            postition_numeric = combination[n][4]
            player_frame = pandas.DataFrame([[second_first_name, second_name, code, position, postition_numeric, total_points, now_costs]],
                                            columns=["second_first_name", "second_name", "code", "position", "postition_numeric", "total_points", "now_costs"])

            PredFrame = PredFrame.append(player_frame)

upload_data_g_sheets(PredFrame, "PredTeam")
PredFrame["RN"] = PredFrame.groupby(["postition_numeric"]).cumcount() + 1
PredFrame.to_csv("PredFrame.csv", index=False)
from DownloadDataFromAPI import json_load_from_url
import pandas
from config_folder.UploadDataToGSheets import upload_data_g_sheets

OverallJSON = json_load_from_url()
# PositionDict
PositionsName = {}
[PositionsName.update({dict.get("id"): dict.get("plural_name")}) for dict in OverallJSON.get("element_types")]

# TeamDict
TeamDict = {}
[TeamDict.update({dict.get("id"): dict.get("name")}) for dict in OverallJSON.get("teams")]

# Get Top 5 Players

PlayersFrame = [stat for stat in OverallJSON.get("elements")]

PlayersFrame = pandas.DataFrame.from_records(PlayersFrame)\
    .groupby(["second_name", "first_name", "id", "element_type", "status", "team", "code", "creativity", "selected_by_percent"])\
    ["total_points", "now_cost", "minutes", "goals_scored", "assists", "bonus", "own_goals", "goals_conceded"].sum()\
    .reset_index()\
    .sort_values("total_points", ascending=False)
PlayersFrame = PlayersFrame[PlayersFrame["status"].str.contains("a|d|i", regex=True)]

PlayersFrame["selected_by_percent"] = PlayersFrame["selected_by_percent"].astype(str).str.replace(".", ",")
PlayersFrame["creativity"] = PlayersFrame["creativity"].astype(str).str.replace(".", ",")
PlayersFrame["RowNumber"] = PlayersFrame.groupby(["element_type"]).cumcount() + 1
PlayersFrame["CountNumber"] = 1
PlayersFrame["Position"] = PlayersFrame["element_type"].map(PositionsName)
PlayersFrame["Team"] = PlayersFrame["team"].map(TeamDict)
upload_data_g_sheets(PlayersFrame.drop(columns=["code"]), sheet_name="PlayersOverall")
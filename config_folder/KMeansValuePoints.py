from sklearn.cluster import KMeans
from DownloadDataFromAPI import json_load_from_url
import pandas
from config_folder.UploadDataToGSheets import upload_data_g_sheets

OverallJSON = json_load_from_url()
PlayersFrame = [stat for stat in OverallJSON.get("elements")]
X = pandas.DataFrame.from_records(PlayersFrame).set_index(["second_name", "first_name"])[["total_points", "now_cost"]]

kmeans = KMeans(n_clusters=4, init='k-means++', max_iter=300, n_init=10, random_state=0)
pred_y = kmeans.fit_predict(X)

upload_data_g_sheets(X.reset_index(), sheet_name="Kmeans")
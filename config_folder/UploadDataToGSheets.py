import pygsheets
from config_folder import config
import os

def upload_data_g_sheets(frame, sheet_name, col_start="A1"):

    client = pygsheets \
        .authorize(client_secret=os.getcwd() \
                                 + "/config_folder/client_secret_736540561403-a2fh6vigick1pfpe94kq6i2s1n6s60rs.apps.googleusercontent.com.json")

    sh = client.open_by_key(config.spreadSheet)
    wks = sh.worksheet_by_title(sheet_name)
    wks.set_dataframe(frame, col_start)
    return "Wykonano"
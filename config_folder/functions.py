from DownloadDataFromAPI import json_load_from_url
import pandas

def player_analize(player_id):

    player_history = pandas\
        .DataFrame.from_records(
        [stat for stat in json_load_from_url(player_id=player_id).get("history")])

    # Last 10 games
    poinst_from_last_10_games = player_history[-10:].groupby(["element"])["total_points"].sum().values[0]

    return poinst_from_last_10_games